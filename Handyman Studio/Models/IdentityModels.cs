﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Handyman_Studio.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int GroupId { get; set; }
        public string Name { get; set; }
        public string Surename { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("GroupId", GroupId.ToString()));

            return userIdentity;
        }
    }

    public static class IdentityExtensions
    {
        public static string GetGroupId(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity) identity).FindFirst("GroupId");
            return claim != null ? claim.Value : string.Empty;
        }
    }

    public class Group
    {
        [Display(Name = "Id grupy:")] public int Id { get; set; }
        [Display(Name = "Nazwa grupy:")] public string Name { get; set; }
    }

    public class Panel
    {
        [Display(Name = "Id panelu:")] public int Id { get; set; }
        [Display(Name = "Nazwa panelu:")] public string Name { get; set; }
    }

    public class GroupPanel
    {
        public int Id { get; set; }
        [Display(Name = "Id grupy:")] public int GroupId { get; set; }
        [Display(Name = "Id panelu:")] public int PanelId { get; set; }
        public bool Read { get; set; }
        public bool Write { get; set; }
    }

    public class Fault
    {
        [Display(Name = "Id usterki:")] public Guid FaultId { get; set; }
        [Display(Name = "Id użytkownika:")] public string UserId { get; set; }
        [Display(Name = "Id serwisanta:")] public string ServicemanId { get; set; }
        [Display(Name = "Data zgłoszenia")] public DateTime FaultReportDate { get; set; }
        [DataType(DataType.Date)]
        [Required]
        [Display(Name = "Data serwisowania:")] public DateTime? FaultRepairDate { get; set; }
        [Display(Name = "Id produktu:")] public int FaultSubject { get; set; }
        [Display(Name = "Opis userki użytkownik:")] public string FaultDescriptionUser { get; set; }
        [Display(Name = "Opis userki serwisant:")] public string FaultDescriptionService { get; set; }
        [Display(Name = "Cena potrzebnych cześć:")] public double FaultPartPrice { get; set; }
        [Display(Name = "Cena naprawy:")] public double FaultRepairPrice { get; set; }
        [Display(Name = "Status naprawy:")] public Status FaultStatus { get; set; }
        [Display(Name = "Kolor:")] public string ThemeColor { get; set; }
    }

    public class Part
    {
        [Display(Name = "Id cześci:")] public int Id { get; set; }
        [Display(Name = "Nazwa cześci:")] public string Name { get; set; }
        [Display(Name = "Ukryte:")] public bool Hidden { get; set; }
        [Display(Name = "Cena netto:")] public double NetPrice { get; set; }
    }

    public class FaultPart
    {
        public int Id { get; set; }
        public Guid FaultId { get; set; }
        public int PartId { get; set; }
    }

    public class FaultReport
    {
        public Fault Fault { get; set; }
        public bool IsSelected { get; set; }
    }

    public class Product
    {
        [Display(Name = "Id produktu:")] public int Id { get; set; }
        [Display(Name = "Producent:")] public string Manufacturer { get; set; }
        [Display(Name = "Model:")] public string Model { get; set; }
        [Display(Name = "Numer seryjny:")] public string SerialNumber { get; set; }
        [Display(Name = "Id właściciela:")] public string OwnerId { get; set; }
    }

    public class Log
    {
        [Display(Name = "Log id:")] public int Id { get; set; }
        [Display(Name = "Data:")] public DateTime OperationDateTime { get; set; }
        [Display(Name = "Id użytkownika:")] public string UserId { get; set; }
        [Display(Name = "Typ operacji:")] public string OperationType { get; set; }
        [Display(Name = "Adres ip:")] public string IpAddress { get; set; }
        [Display(Name = "Konto:")] public string InvolvedUserAccount { get; set; }
        [Display(Name = "Id usterki:")] public Guid FaultId { get; set; }
        [Display(Name = "Id części:")] public int PartId { get; set; }
        [Display(Name = "Id produktu")] public int ProductId { get; set; }
    }

    public class Events
    {
        [Display(Name = "ID:")] public int Id { get; set; }
        [Display(Name = "Zdarzenie:")] public string Subject { get; set; }
        [Display(Name = "Opis:")] public string Description { get; set; }
        [Display(Name = "Start:")] public DateTime Start { get; set; }
        [Display(Name = "Koniec:")] public DateTime End { get; set; }
        [Display(Name = "Kolor:")] public string ThemeColor { get; set; }
        [Display(Name = "Całodniowe:")] public Boolean IsFullDay { get; set; }
    }

    public class ProductFaultModel
    {
        public Fault Fault { get; set; }
        public int SelectedProduct { get; set; }
        public List<Product> UserProduct { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {
        }

        public DbSet<Log> Logs { get; set; }
        public DbSet<Panel> Panels { get; set; }
        public DbSet<GroupPanel> GroupsPanels { get; set; }
        public DbSet<Fault> Faults { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<FaultPart> FaultsParts { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Events> CalendarEvent { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}