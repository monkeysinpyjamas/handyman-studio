﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Handyman_Studio.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Adres Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public enum Status
    {
        Open,
        Working,
        Closed
    }

    public class UserPanels
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
    }

    public class UserIndexModel
    {
        public string Id { get; set; }

        [Display(Name = "Grupa")]
        public string GroupName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Imie")]
        public string Name { get; set; }

        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }

        [Display(Name = "Numer telefonu")]
        public string PhoneNumber { get; set; }
    }

    public class UserIndex
    {
        public bool Write { get; set; }
        public List<UserIndexModel> Users { get; set; }
    }

    public class UserModel
    {
        public string Id { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Powtórz hasło")]
        public string ConfirmedPassword { get; set; }

        [Display(Name = "Numer telefonu")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Grupa")]
        public int GroupId { get; set; }

        public List<Group> AllGroups { get; set; }
    }

    public class GroupModel
    {
        public int Id { get; set; }

        [Display(Name = "Grupa")]
        public string Name { get; set; }

        public List<int> Access { get; set; }
        public List<Panel> AllPanels { get; set; }
    }

    public class GroupIndex
    {
        public bool Write { get; set; }
        [Display(Name = "Nazwa grupy:")] public List<Group> Groups { get; set; }
    }

    public class GroupDeleteModel
    {
        public int Id { get; set; }

        [Display(Name = "Grupa")]
        public string Name { get; set; }

        public int MovedToId { get; set; }
        public List<Group> AllGroups { get; set; }
    }

    public class FaultEditModel
    {
        public Guid FaultId { get; set; }
        public DateTime FaultRepairDate { get; set; }
        public double FaultRepairPrice { get; set; }
        public Status FaultStatus { get; set; }
        public List<Part> FaultParts { get; set; }
    }

    public class FaultPartsModel
    {
        public List<Part> AllParts { get; set; }
        public List<Part> FaultParts { get; set; }
        public Guid FaultId { get; set; }
        public int PartId { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required] public string Provider { get; set; }

        [Required]
        [Display(Name = "Kod dostępu")]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "Zapamiętaj tą przeglądarkę")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Adres Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Adres Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Zapamietaj dane logowania")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Adres Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} musi mieć co najmniej {2} znaków.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage =
            "Powtórzone hasło musi być takie samo.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Nazwisko")]
        public string Surename { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [Display(Name = "Numer telefonu")]
        public string Phonenumber { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Adres Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} musi mieć co najmniej {2} znaków.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage =
            "Powtórzone hasło musi być takie samo.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}