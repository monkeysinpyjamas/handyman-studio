﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;

namespace Handyman_Studio.Controllers
{
    public class LogsController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public async Task<ActionResult> Index()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Logs))
                return RedirectToAction("Index", "Home");

            return View(await db.Logs.ToListAsync());
        }

        public FileResult GenerateLogs()
        {
            const string separator = " |\t";
            var logs = db.Logs.ToList();
            var output = "Id" + separator +
                         "OperationDateTime" + separator +
                         "UserId" + separator +
                         "OperationType" + separator +
                         "IpAddress" + separator +
                         "InvolvedUserAccount" + separator +
                         "FaultId" + separator +
                         "PartId" + separator +
                         "ProductId" + " |\r\n\r\n";

            foreach (var log in logs)
            {
                output += log.Id + separator;
                output += log.OperationDateTime + separator;
                output += log.UserId + separator;
                output += log.OperationType + separator;
                output += log.IpAddress + separator;
                output += log.InvolvedUserAccount + separator;
                output += log.FaultId + separator;
                output += log.PartId + separator;
                output += log.ProductId + " |\r\n";
            }

            var byteArray = Encoding.UTF8.GetBytes(output);
            var stream = new MemoryStream(byteArray);
            var databaseLogName = @"HMSDatabaseLog " + DateTime.Now + ".txt";
            return File(stream, "text/plain", databaseLogName);
        }
    }
}