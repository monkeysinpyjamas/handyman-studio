﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;

namespace Handyman_Studio.Controllers
{
    public class GroupsController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Groups
        public ActionResult Index()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Groups))
                return RedirectToAction("Index", "Home");


            var groupId = db.Users.Where(u => u.Email == User.Identity.Name).FirstOrDefault().GroupId;
            var access = db.GroupsPanels.Where(gp => gp.GroupId == groupId && gp.PanelId == PanelId.Groups).FirstOrDefault().Write;

            var model = new GroupIndex
            {
                Write = access,
                Groups = db.Groups.ToList()
            };

            return View(model);
        }

        // GET: Groups/Details/5
        public ActionResult Details(int? id)
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Groups))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var group = db.Groups.Find(id);
            if (group == null) return HttpNotFound();

            var model = new GroupModel
            {
                Id = group.Id,
                Name = group.Name,
                AllPanels = db.Panels.ToList(),
                Access = new List<int>()
            };
            var grouppanels = db.GroupsPanels.Where(gp => gp.GroupId == group.Id).OrderBy(gp => gp.PanelId).ToList();
            grouppanels.ForEach(gp => model.Access.Add((gp.Write ? 1 : 0) + (gp.Read ? 1 : 0)));

            return View(model);
        }

        // GET: Groups/Create
        public ActionResult Create()
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Groups))
                return RedirectToAction("Index", "Home");

            var panels = db.Panels.ToList();
            var access = new List<int>();

            panels.ForEach(p => access.Add(0));

            var model = new GroupModel
            {
                Access = access,
                AllPanels = panels
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GroupModel model)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Groups))
                return RedirectToAction("Index", "Home");

            var group = new Group {Name = model.Name};

            if (model.Name == null)
            {
                ModelState.AddModelError("", "Pole nazwa grupy nie może być pusta");
                return View(model);
            }

            if (db.Groups.FirstOrDefault(g => g.Name == model.Name) != null)
            {
                ModelState.AddModelError("", "Grupa już istnieje");
                return View(model);
            }

            db.Groups.Add(group);
            db.SaveChanges();

            var id = db.Groups.Where(g => g.Name == model.Name).FirstOrDefault().Id;

            for (var i = 0; i < model.Access.Count; i++)
            {
                var read = model.Access[i] > 0;
                var write = model.Access[i] > 1;

                var grouppanel = new GroupPanel {GroupId = id, PanelId = i + 1, Read = read, Write = write};
                db.GroupsPanels.Add(grouppanel);
            }

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        // GET: Groups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Groups))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var group = db.Groups.Find(id);
            if (group == null) return HttpNotFound();

            var model = new GroupModel
            {
                Id = group.Id,
                Name = group.Name,
                Access = new List<int>(),
                AllPanels = db.Panels.ToList()
            };

            var grouppanels = db.GroupsPanels.Where(gp => gp.GroupId == group.Id).OrderBy(gp => gp.PanelId).ToList();
            grouppanels.ForEach(gp => model.Access.Add((gp.Write ? 1 : 0) + (gp.Read ? 1 : 0)));

            return View(model);
        }

        // POST: Groups/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GroupModel model)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Groups))
                return RedirectToAction("Index", "Home");

            if (model.Name == null)
                ModelState.AddModelError("", "Pole nazwa grupy nie może być pusta");

            var group = new Group {Id = model.Id, Name = model.Name};
            if (ModelState.IsValid)
            {
                db.Entry(group).State = EntityState.Modified;
                db.SaveChanges();

                var grouppanels = db.GroupsPanels.Where(gp => gp.GroupId == group.Id).ToList()
                    .OrderBy(gp => gp.PanelId);

                foreach (var gp in grouppanels)
                {
                    gp.Read = model.Access[gp.PanelId - 1] > 0;
                    gp.Write = model.Access[gp.PanelId - 1] > 1;

                    db.Entry(gp).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Groups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Groups))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var group = db.Groups.Find(id);
            if (group == null) return HttpNotFound();

            var model = new GroupDeleteModel
            {
                Id = group.Id,
                Name = group.Name,
                AllGroups = db.Groups.ToList()
            };

            return View(model);
        }

        // POST: Groups/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(GroupDeleteModel model)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Groups))
                return RedirectToAction("Index", "Home");

            if (model.Id == model.MovedToId)
            {
                ModelState.AddModelError("", "Nie można przenieść do aktualnie usuwanej grupy");
                return View(model);
            }

            var group = db.Groups.Find(model.Id);
            db.Groups.Remove(group);
            db.GroupsPanels.Where(gp => gp.GroupId == model.Id).ToList().ForEach(gp => db.GroupsPanels.Remove(gp));

            db.Users.Where(u => u.GroupId == model.Id).ToList().ForEach(u =>
            {
                u.GroupId = model.MovedToId;
                db.Entry(u).State = EntityState.Modified;
                db.SaveChanges();
            });

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}