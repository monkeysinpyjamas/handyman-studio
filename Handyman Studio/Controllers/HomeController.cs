﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;

namespace Handyman_Studio.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Opis strony firmy.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Kontakt do firmy.";

            return View();
        }

        public ActionResult CompanyOffer()
        {
            ViewBag.Message = "Oferta firmy.";

            return View();
        }

        public ActionResult PanelsIndex()
        {
            var bindings = new Dictionary<int, Tuple<string, string, string>>
            {
                {PanelId.FaultRegistration, new Tuple<string, string, string>("Faults", "UserIndex", "Zgłoszenie usterki.png")},
                {PanelId.Users, new Tuple<string, string, string>("ApplicationUsers", "Index", "Użytkownicy.png")},
                {PanelId.Groups, new Tuple<string, string, string>("Groups", "Index", "Grupy.png")},
                {PanelId.Products, new Tuple<string, string, string>("Products", "Index", "Urządzenia.png")},
                {PanelId.Faults, new Tuple<string, string, string>("Faults", "Index", "Usterki.png")},
                {PanelId.Parts, new Tuple<string, string, string>("Parts", "Index", "Części.png")},
                {PanelId.Accounting, new Tuple<string, string, string>("AccountantPanel", "Index", "Księgowość.png")},
                {PanelId.Logs, new Tuple<string, string, string>("Logs", "Index", "Księgowość.png")},
                {PanelId.Calendar, new Tuple<string, string, string>("Calendar", "Index", "Księgowość.png")}
            };

            var group = db.Users.First(user => user.Email == User.Identity.Name).GroupId;
            var userpanels = db.GroupsPanels
                .Where(g => g.GroupId == group && g.Read)
                .Join(db.Panels, gp => gp.PanelId, p => p.Id, (gp, p) => new { p.Name, p.Id })
                .ToList();

            if (userpanels.Count() == 1)
                return RedirectToAction(bindings[userpanels[0].Id].Item2, bindings[userpanels[0].Id].Item1);

            RedirectToAction("", "");

            var model = new List<UserPanels>();
            userpanels.ForEach(up => model.Add(new UserPanels
            {
                Name = up.Name,
                Address = bindings[up.Id].Item1 + "/" + bindings[up.Id].Item2,
                Image = bindings[up.Id].Item3
            }));

            return View(model);
        }
    }
}