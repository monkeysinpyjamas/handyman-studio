﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;

namespace Handyman_Studio.Controllers
{
    public class PartsController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Parts
        public ActionResult Index()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Parts))
                return RedirectToAction("Index", "Home");

            return View(db.Parts.ToList());
        }

        // GET: Parts/Details/5
        public ActionResult Details(int? id)
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Parts))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var part = db.Parts.Find(id);
            if (part == null) return HttpNotFound();
            return View(part);
        }

        // GET: Parts/Create
        public ActionResult Create()
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Parts))
                return RedirectToAction("Index", "Home");

            return View();
        }

        // POST: Parts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Hidden,NetPrice")]
            Part part)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Parts))
                return RedirectToAction("Index", "Home");

            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Part creation",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                PartId = part.Id
            };

            if (!ModelState.IsValid)
            {
                db.Logs.Add(log);
                db.SaveChanges();
                return View(part);
            }

            db.Logs.Add(log);
            db.Parts.Add(part);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Parts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Parts))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var part = db.Parts.Find(id);
            if (part == null) return HttpNotFound();
            return View(part);
        }

        // POST: Parts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Hidden,NetPrice")]
            Part part)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Parts))
                return RedirectToAction("Index", "Home");

            if (!ModelState.IsValid) return View(part);

            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Part edit",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                PartId = part.Id
            };
            db.Logs.Add(log);
            db.Entry(part).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Parts/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Parts))
                return RedirectToAction("Index", "Home");

            var part = db.Parts.Find(id);
            if (part != null)
            {
                db.Parts.Remove(part);
                var log = new Log
                {
                    Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                    InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                    OperationDateTime = DateTime.Now,
                    OperationType = @"Part delete",
                    IpAddress = GetIpAddress(),
                    UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                    PartId = part.Id
                };

                db.Logs.Add(log);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }


        private string GetIpAddress()
        {
            var context = System.Web.HttpContext.Current;
            var ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ipAddress)) return context.Request.ServerVariables["REMOTE_ADDR"];
            var addresses = ipAddress.Split(',');
            return addresses.Length != 0 ? addresses[0] : context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}