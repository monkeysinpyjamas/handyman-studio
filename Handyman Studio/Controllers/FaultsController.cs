﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;
using Microsoft.AspNet.Identity;

namespace Handyman_Studio.Controllers
{
    [Authorize]
    public class FaultsController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Faults
        public ActionResult Index()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            var userId = db.Users.First(u => u.UserName == User.Identity.Name).Id;
            return View(db.Faults.Where(f => f.FaultStatus == 0 || f.ServicemanId == userId).ToList());
        }

        // GET: Faults/Details/5
        public ActionResult Details(Guid? id)
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var fault = db.Faults.Find(id);
            if (fault == null) return HttpNotFound();
            return View(fault);
        }

        // GET: Faults/Create
        public ActionResult Create()
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            var userId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id;
            var model = new ProductFaultModel {UserProduct = db.Products.Where(p => p.OwnerId == userId).ToList()};

            return View(model);
        }

        // POST: Faults/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductFaultModel productFault)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            var user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            var userId = user.Id;
            var userProducts = db.Products.Where(p => p.OwnerId == userId).ToList();

            productFault.Fault.FaultId = Guid.NewGuid();
            productFault.Fault.UserId = userId;
            productFault.Fault.ServicemanId = "Nie przydzielono";
            productFault.Fault.FaultStatus = Status.Open;
            productFault.Fault.FaultPartPrice = 0.0;
            productFault.Fault.FaultRepairPrice = 0.0;
            productFault.Fault.FaultSubject = productFault.SelectedProduct;
            productFault.Fault.FaultReportDate = DateTime.Now;
            productFault.Fault.FaultDescriptionService = "brak";
            productFault.UserProduct = userProducts;

            if (productFault.SelectedProduct == 0 || productFault.Fault.FaultRepairDate == null)
                return View(productFault);

            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(u => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Fault creation",
                IpAddress = GetIpAddress(),
                UserId = userId,
                FaultId = productFault.Fault.FaultId,
                ProductId = productFault.SelectedProduct
            };

            db.Logs.Add(log);
            db.Faults.Add(productFault.Fault);
            db.SaveChanges();
            return RedirectToAction(user.GroupId == GroupId.Client ? "UserIndex" : "Index");
        }

        // GET: Faults/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var fault = db.Faults.Find(id);
            if (fault == null) throw new InvalidOperationException();


            var faultedit = new FaultEditModel
            {
                FaultId = fault.FaultId,
                FaultRepairDate = (DateTime) fault.FaultRepairDate,
                FaultRepairPrice = fault.FaultRepairPrice,
                FaultStatus = fault.FaultStatus,
                FaultParts = db.FaultsParts
                    .Where(fp => fp.FaultId == fault.FaultId)
                    .Join(db.Parts, fp => fp.PartId, p => p.Id, (fp, p) => p)
                    .ToList()
            };

            return View(faultedit);
        }

        // POST: Faults/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FaultId,FaultRepairDate,FaultRepairPrice,FaultStatus")]
            FaultEditModel model)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            var fault = db.Faults.Find(model.FaultId);

            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Faults edit failure",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                FaultId = fault.FaultId,
                ProductId = fault.FaultSubject
            };

            fault.FaultRepairDate = model.FaultRepairDate;
            fault.FaultRepairPrice = model.FaultRepairPrice;
            fault.FaultStatus = model.FaultStatus;

            if (!ModelState.IsValid) return View(model);

            log.OperationType = @"Faults edit success";
            db.Logs.Add(log);
            db.Entry(fault).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Claim(Guid id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            var fault = db.Faults.Find(id);
            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Claim fault failure",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                FaultId = id,
                ProductId = fault.FaultSubject
            };

            fault.ServicemanId = db.Users.First(u => u.UserName == User.Identity.Name).Id;
            fault.FaultStatus = Status.Working;

            if (!ModelState.IsValid)
            {
                db.Logs.Add(log);
                db.SaveChanges();
                return View("Index");
            }

            log.OperationType = @"Claim fault success";
            db.Logs.Add(log);
            db.Entry(fault).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClaimFault(Guid id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            var fault = db.Faults.Find(id);
            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Claim fault failure",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                FaultId = id,
                ProductId = fault.FaultSubject
            };

            log.OperationType = @"Claim fault success";
            fault.ServicemanId = db.Users.First(m => m.UserName == User.Identity.Name).Id;
            db.Entry(fault).State = EntityState.Modified;

            db.Logs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Parts(FaultPartsModel model, string returnUrl)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            if (model.PartId != 0)
            {
                db.FaultsParts.Add(new FaultPart {FaultId = model.FaultId, PartId = model.PartId});
                db.SaveChanges();

                var fault = db.Faults.Find(model.FaultId);

                var sum = db.FaultsParts
                    .Join(db.Parts, fp => fp.PartId, p => p.Id, (fp, p) => new {fp.FaultId, Price = p.NetPrice})
                    .Where(el => el.FaultId == fault.FaultId)
                    .Sum(p => p.Price);

                if (fault != null)
                {
                    fault.FaultPartPrice = sum;
                    db.Entry(fault).State = EntityState.Modified;
                }

                db.SaveChanges();
                model.PartId = 0;
            }

            model.FaultParts = db.FaultsParts
                .Where(fp => fp.FaultId == model.FaultId)
                .Join(db.Parts, fp => fp.PartId, p => p.Id, (fp, p) => p)
                .ToList();

            model.AllParts = db.Parts.Where(p => !p.Hidden).ToList();

            return View(model);
        }

        public ActionResult DeletePart(FaultPartsModel model, string returnUrl)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Faults))
                return RedirectToAction("Index", "Home");

            var toRemove = db.FaultsParts.First(fp => fp.FaultId == model.FaultId && fp.PartId == model.PartId);
            db.FaultsParts.Remove(toRemove);
            db.SaveChanges();

            var fault = db.Faults.Find(model.FaultId);

            var joined = db.FaultsParts
                .Join(db.Parts, fp => fp.PartId, p => p.Id, (fp, p) => new {model.FaultId, Price = p.NetPrice})
                .ToList();

            var result = joined.Where(el => el.FaultId == fault.FaultId);

            double sum = 0;
            if (result != null)
                sum = result.Sum(p => p.Price);

            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Delete part from fault failure",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                PartId = model.PartId,
                FaultId = fault.FaultId,
                ProductId = fault.FaultSubject
            };

            fault.FaultPartPrice = sum;
            db.Entry(fault).State = EntityState.Modified;
            log.OperationType = @"Delete part from fault success";

            db.Logs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
        
        public ActionResult UserIndex()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.FaultRegistration))
                return RedirectToAction("Index", "Home");

            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            var userFaults = new List<Fault>();

            foreach (var fault in db.Faults.ToList())
                if (fault.UserId == userId)
                    userFaults.Add(fault);

            return View(userFaults);
        }

        public ActionResult UserDetails(Guid? id)
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.FaultRegistration))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var fault = db.Faults.Find(id);
            if (fault == null) return HttpNotFound();
            return View(fault);
        }

        private static string GetIpAddress()
        {
            var context = System.Web.HttpContext.Current;
            var ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ipAddress)) return context.Request.ServerVariables["REMOTE_ADDR"];
            var addresses = ipAddress.Split(',');
            return addresses.Length != 0 ? addresses[0] : context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}