﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Handyman_Studio.Utils;

namespace Handyman_Studio.Controllers
{
    public class ApplicationUsersController : Controller
    {
        private readonly ApplicationDbContext db;
        private ApplicationUserManager _userManager;

        public ApplicationUsersController()
        {
            db = new ApplicationDbContext();
        }

        public ApplicationUserManager UserManager
        {
            get => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

        // GET: ApplicationUsers
        public ActionResult Index()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Users))
                return RedirectToAction("Index", "Home");

            var model = new List<UserIndexModel>();
            db.Users.ToList().ForEach(m => model.Add(new UserIndexModel
            {
                Id = m.Id,
                Email = m.Email,
                PhoneNumber = m.PhoneNumber,
                GroupName = db.Groups.Where(g => g.Id == m.GroupId).FirstOrDefault().Name,
                Name = m.Name,
                Surname = m.Surename
            }));

            var groupId = db.Users.Where(u => u.Email == User.Identity.Name).FirstOrDefault().GroupId;
            var access = db.GroupsPanels.Where(gp => gp.GroupId == groupId && gp.PanelId == PanelId.Users).FirstOrDefault().Write;

            var returnModel = new UserIndex()
            {
                Write = access,
                Users = model
            };


            return View(returnModel);
        }

        // GET: ApplicationUsers/Details/5
        public ActionResult Details(string id)
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Users))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var applicationUser = db.Users.Find(id);
            if (applicationUser == null) return HttpNotFound();
            return PartialView(applicationUser);
        }

        // GET: ApplicationUsers/Create
        public ActionResult Create()
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Users))
                return RedirectToAction("Index", "Home");

            var model = new UserModel
            {
                AllGroups = db.Groups.ToList()
            };

            return View(model);
        }

        // POST: ApplicationUsers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserModel model)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Users))
                return RedirectToAction("Index", "Home");

            var PasswordHash = new PasswordHasher();

            if (model.Password == null)
                model.Password = "";

            if (model.ConfirmedPassword == null)
                model.ConfirmedPassword = "";

            if (model.Password != model.ConfirmedPassword)
            {
                ModelState.AddModelError("", "Powtórzone hasło nie jest prawidłowe");
                return View(model);
            }

            var applicationUser = new ApplicationUser
            {
                Email = model.Email,
                UserName = model.Email,
                GroupId = model.GroupId,
                PhoneNumber = model.PhoneNumber
            };

            if (!ModelState.IsValid) return View(model);

            var result = UserManager.Create(applicationUser, model.Password);
            if (result.Succeeded) return RedirectToAction("Index");

            foreach (var error in result.Errors) ModelState.AddModelError("", error);

            return View(model);
        }

        // GET: ApplicationUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Users))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var applicationUser = db.Users.Find(id);
            if (applicationUser == null) return HttpNotFound();

            var model = new UserModel
            {
                Id = applicationUser.Id,
                Email = applicationUser.Email,
                GroupId = applicationUser.GroupId,
                AllGroups = db.Groups.ToList()
            };

            return View(model);
        }

        // POST: ApplicationUsers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserModel model)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Users))
                return RedirectToAction("Index", "Home");

            var PasswordHash = new PasswordHasher();
            var applicationUser = db.Users.FirstOrDefault(user => user.Id == model.Id);

            if (model.Password == null)
                model.Password = "";

            if (model.ConfirmedPassword == null)
                model.ConfirmedPassword = "";

            if (model.Password != model.ConfirmedPassword)
            {
                ModelState.AddModelError("", "Powtórzone hasło nie jest prawidłowe");
                return View(model);
            }

            if (model.Password != "") applicationUser.PasswordHash = PasswordHash.HashPassword(model.Password);

            applicationUser.Email = model.Email;
            applicationUser.GroupId = model.GroupId;
            applicationUser.PhoneNumber = model.PhoneNumber;

            if (ModelState.IsValid)
            {
                db.Entry(applicationUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: ApplicationUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Users))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var applicationUser = db.Users.Find(id);
            if (applicationUser == null) return HttpNotFound();
            return View(applicationUser);
        }

        // POST: ApplicationUsers/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Users))
                return RedirectToAction("Index", "Home");

            var applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}