﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;
using IronPdf;

namespace Handyman_Studio.Controllers
{
    public class AccountantPanelController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Accounting))
                return RedirectToAction("Index", "Home");

            var faultPartList = db.Faults.Select(fault => new FaultReport {Fault = fault, IsSelected = false});
            return View(faultPartList.ToList());
        }

        [HttpGet]
        public ActionResult Details(Guid? id)
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Accounting))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var fault = db.Faults.Find(id);
            if (fault == null) return HttpNotFound();
            return View(fault);
        }

        [HttpPost]
        public FileResult GenerateSingleReport(Guid selectedObject)
        {
            var fault = db.Faults.Find(selectedObject);
            var faultPartList = new List<Part>();
            var faultPart = db.FaultsParts.ToList();
            var renderer = new HtmlToPdf();

            foreach (var element in faultPart)
                if (element.FaultId == fault.FaultId)
                    faultPartList.Add(db.Parts.Find(element.PartId));

            var reportName = @"HMSFaultReport " + DateTime.Now + ".pdf";
            var user = db.Users.Find(fault.UserId);
            var client = new TransportData.ClientData
            {
                Name = user.Name,
                Surname = user.Surename,
                PhoneNumer = user.PhoneNumber,
                Mail = user.Email
            };
            return File(renderer.RenderHtmlAsPdf(HtmlGenerator.HtmlForFault(client,fault, faultPartList)).BinaryData,
                "application/pdf", reportName);
        }

        [HttpGet]
        public ActionResult GenerateCollectiveReport()
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Accounting))
                return RedirectToAction("Index", "Home");

            var faultPartList = db.Faults.Select(fault => new FaultReport {Fault = fault, IsSelected = false});
            return View(faultPartList.ToList());
        }

        [HttpPost]
        public FileResult GenerateCollectiveReport(Guid[] selectedObjects)
        {
            if (selectedObjects == null)
                return null;

            var faultList = selectedObjects.Select(id => db.Faults.Find(id)).ToList();
            var faultPartList = new List<Part>();

            var faultPart = db.FaultsParts.ToList();

            var renderer = new HtmlToPdf();
            var reports = new Queue<PdfDocument>();

            foreach (var fault in faultList)
            {
                foreach (var element in faultPart)
                    if (element.FaultId == fault.FaultId)
                        faultPartList.Add(db.Parts.Find(element.PartId));

                var user = db.Users.Find(fault.UserId);
                var client = new TransportData.ClientData
                {
                    Name = user.Name,
                    Surname = user.Surename,
                    PhoneNumer = user.PhoneNumber,
                    Mail = user.Email
                };

                reports.Enqueue(renderer.RenderHtmlAsPdf(HtmlGenerator.HtmlForFault(client, fault, faultPartList)));
            }

            var mergedPdfs = PdfDocument.Merge(reports);
            var reportName = @"HMSFaultsReport " + DateTime.Now + ".pdf";
            return File(mergedPdfs.BinaryData, "application/pdf", reportName);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}