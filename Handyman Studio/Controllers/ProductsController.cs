﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;

namespace Handyman_Studio.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Products
        public ActionResult Index()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Products))
                return RedirectToAction("Index", "Home");

            var id = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name)?.Id;
            var groupid = db.Users.Find(id).GroupId;
            
            return View(db.Products.Where(p => p.OwnerId == id || groupid != GroupId.Client).ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Products))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var product = db.Products.Find(id);
            if (product == null) return HttpNotFound();
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Products))
                return RedirectToAction("Index", "Home");

            return View();
        }

        // POST: Products/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Manufacturer,Model,SerialNumber,OwnerId")]
            Product product)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Products))
                return RedirectToAction("Index", "Home");

            product.OwnerId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id;
            if (!ModelState.IsValid) return View(product);

            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Product creation",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                ProductId = product.Id
            };

            db.Logs.Add(log);
            db.Products.Add(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Products))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var product = db.Products.Find(id);
            if (product == null) return HttpNotFound();
            return View(product);
        }

        // POST: Products/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Manufacturer,Model,SerialNumber,OwnerId")]
            Product product)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Products))
                return RedirectToAction("Index", "Home");

            if (!ModelState.IsValid) return View(product);
            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Product edit",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                ProductId = product.Id
            };

            db.Logs.Add(log);
            db.Products.Add(product);
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Products))
                return RedirectToAction("Index", "Home");

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var product = db.Products.Find(id);
            if (product == null) return HttpNotFound();
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!AccessCheck.Write(User.Identity.Name, PanelId.Products))
                return RedirectToAction("Index", "Home");

            var product = db.Products.Find(id);
            db.Products.Remove(product);
            var log = new Log
            {
                Id = Math.Abs(DateTime.Now.GetHashCode() * DateTime.Now.Millisecond),
                InvolvedUserAccount = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Email,
                OperationDateTime = DateTime.Now,
                OperationType = @"Product delete",
                IpAddress = GetIpAddress(),
                UserId = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name)?.Id,
                ProductId = product.Id
            };

            db.Logs.Add(log);
            db.Products.Add(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }

        private static string GetIpAddress()
        {
            var context = System.Web.HttpContext.Current;
            var ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ipAddress)) return context.Request.ServerVariables["REMOTE_ADDR"];
            var addresses = ipAddress.Split(',');
            return addresses.Length != 0 ? addresses[0] : context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}