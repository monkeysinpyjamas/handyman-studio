﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;

namespace Handyman_Studio.Controllers
{
    public class CalendarController : Controller
    {
        // GET: Calendar
        public ActionResult Index()
        {
            if (!AccessCheck.Read(User.Identity.Name, PanelId.Calendar))
                return RedirectToAction("Index", "Home");

            return View();
        }

        public JsonResult GetEvents()
        {
            using (var db = new ApplicationDbContext())
            {
                var events = new List<object>();

                var id = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name)?.Id;
                var groupid = db.Users.Find(id).GroupId;

                var faultsEvents = db.Faults.ToList();

                foreach (var tmp in faultsEvents)
                    if (groupid == GroupId.Manager || groupid == GroupId.Administrator)
                    {
                        if (tmp.FaultStatus == Status.Closed)
                            tmp.ThemeColor = "green";
                        else if (tmp.FaultStatus == Status.Open)
                            tmp.ThemeColor = "blue";
                        else if (tmp.FaultStatus == Status.Working)
                            tmp.ThemeColor = "yellow";

                        events.Add(tmp);
                    }
                    else if (tmp.UserId == id || tmp.ServicemanId == id)
                    {
                        if (tmp.FaultStatus == Status.Closed)
                            tmp.ThemeColor = "green";
                        else if (tmp.FaultStatus == Status.Open)
                            tmp.ThemeColor = "blue";
                        else if (tmp.FaultStatus == Status.Working)
                            tmp.ThemeColor = "yellow";

                        events.Add(tmp);
                    }

                return new JsonResult {Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
            }
        }
    }
}