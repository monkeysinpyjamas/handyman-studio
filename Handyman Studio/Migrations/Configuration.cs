using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Handyman_Studio.Models;
using Handyman_Studio.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Handyman_Studio.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var passwordHash = new PasswordHasher();

            if (!context.Users.Any(u => u.UserName == "admin@admin.net"))
            {
                var user = new ApplicationUser
                {
                    Name = "Administrator",
                    Surename = "AdministatorNazwisko",
                    PhoneNumber = "777777777",
                    UserName = "admin@admin.net",
                    Email = "admin@admin.net",
                    PasswordHash = passwordHash.HashPassword("123456"),
                    GroupId = GroupId.Administrator
                };

                userManager.Create(user);
            }

            if (!context.Users.Any(u => u.UserName == "kierownik@kierownik.net"))
            {
                var user = new ApplicationUser
                {
                    Name = "Kierownik",
                    Surename = "KierownikNazwisko",
                    PhoneNumber = "77777777",
                    UserName = "kierownik@kierownik.net",
                    Email = "kierownik@kierownik.net",
                    PasswordHash = passwordHash.HashPassword("123456"),
                    GroupId = GroupId.Manager
                };

                userManager.Create(user);
            }

            if (!context.Users.Any(u => u.UserName == "ksiegowa@ksiegowa.net"))
            {
                var user = new ApplicationUser
                {
                    Name = "Księgowa",
                    Surename = "KsięgowaNazwisko",
                    PhoneNumber = "777777777",
                    UserName = "ksiegowa@ksiegowa.net",
                    Email = "ksiegowa@ksiegowa.net",
                    PasswordHash = passwordHash.HashPassword("123456"),
                    GroupId = GroupId.Accountant
                };

                userManager.Create(user);
            }

            if (!context.Users.Any(u => u.UserName == "serwisant@serwisant.net"))
            {
                var user = new ApplicationUser
                {
                    Name = "Serwisant",
                    Surename = "SerwisantNazwisko",
                    PhoneNumber = "555555555",
                    UserName = "serwisant@serwisant.net",
                    Email = "serwisant@serwisant.net",
                    PasswordHash = passwordHash.HashPassword("123456"),
                    GroupId = GroupId.Serviceman
                };

                userManager.Create(user);
            }

            if (!context.Users.Any(u => u.UserName == "klient@klient.net"))
            {
                var user = new ApplicationUser
                {
                    Name = "Klient",
                    Surename = "KlientNazwisko",
                    PhoneNumber = "333333333",
                    UserName = "klient@klient.net",
                    Email = "klient@klient.net",
                    PasswordHash = passwordHash.HashPassword("123456"),
                    GroupId = GroupId.Client
                };

                userManager.Create(user);
            }

            var groups = new List<Group>
            {
                new Group {Id = GroupId.Administrator,
                    Name = "Administrator"},
                new Group {Id = GroupId.Manager,
                    Name = "Kierownik"},
                new Group {Id = GroupId.Accountant,
                    Name = "Księgowa"},
                new Group {Id = GroupId.Serviceman,
                    Name = "Serwisant"},
                new Group {Id = GroupId.Client,
                    Name = "Klient"}
            };

            groups.ForEach(s => context.Groups.AddOrUpdate(p => p.Id, s));

            var panels = new List<Panel>
            {
                new Panel {Id = PanelId.FaultRegistration,
                    Name = "Zgłoszenie usterki"},
                new Panel {Id = PanelId.Users,
                    Name = "Użytkownicy"},
                new Panel {Id = PanelId.Groups,
                    Name = "Grupy"},
                new Panel {Id = PanelId.Products,
                    Name = "Urządzenia"},
                new Panel {Id = PanelId.Faults,
                    Name = "Usterki"},
                new Panel {Id = PanelId.Parts,
                    Name = "Części"},
                new Panel {Id = PanelId.Accounting,
                    Name = "Księgowość"},
                new Panel {Id = PanelId.Logs,
                    Name = "Logi bazy danych"},
                new Panel {Id = PanelId.Calendar,
                    Name = "Terminarz"}
            };

            panels.ForEach(s => context.Panels.AddOrUpdate(p => p.Id, s));

            var groupPanels = new List<GroupPanel>
            {
                /*
                 * ADMINISTRATOR
                 */
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.FaultRegistration,
                    Read = true,Write = true},
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.Users,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.Groups,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.Products,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.Faults,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.Parts,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.Accounting,
                    Read = true,Write = true},
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.Logs,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Administrator, PanelId = PanelId.Calendar,
                    Read = true, Write = true},
                /*
                 * KIEROWNIK
                 */
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.FaultRegistration,
                    Read = false,Write = false},
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.Users,
                    Read = false, Write = false},
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.Groups,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.Products,
                    Read = false, Write = false},
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.Faults,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.Parts,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.Accounting,
                    Read = true, Write = true},
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.Logs,
                    Read = false, Write = false},
                new GroupPanel {GroupId = GroupId.Manager, PanelId = PanelId.Calendar,
                    Read = true, Write = true},
                /*
                 * KSIĘGOWA
                 */
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.FaultRegistration,
                    Read = false,Write = false},  
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.Users,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.Groups,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.Products,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.Faults,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.Parts,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.Accounting,
                    Read = true,Write = true},    
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.Logs,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Accountant, PanelId = PanelId.Calendar,
                    Read = false, Write = false},
                /*
                 * SERWISANT
                 */
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.FaultRegistration,
                    Read = false,Write = false},  
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.Users,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.Groups,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.Products,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.Faults,
                    Read = true, Write = true},   
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.Parts,
                    Read = true, Write = true},   
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.Accounting,
                    Read = false,Write = false},  
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.Logs,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Serviceman, PanelId = PanelId.Calendar,
                    Read = true, Write = true},
                /*
                 * KLIENT
                 */
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.FaultRegistration,
                    Read = true,Write = true},   
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.Users,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.Groups,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.Products,
                    Read = true, Write = false},  
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.Faults,
                    Read = true, Write = true},   
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.Parts,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.Accounting,
                    Read = false,Write = false},  
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.Logs,
                    Read = false, Write = false}, 
                new GroupPanel {GroupId = GroupId.Client, PanelId = PanelId.Calendar,
                    Read = true, Write = true},
            };

            groupPanels.ForEach(s => context.GroupsPanels.AddOrUpdate(p => p.Id, s));
            context.SaveChanges();
        }
    }
}