﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Handyman_Studio.Startup))]
namespace Handyman_Studio
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
