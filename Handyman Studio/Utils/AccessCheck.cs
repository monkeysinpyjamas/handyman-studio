﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Handyman_Studio.Models;
using Microsoft.AspNet.Identity;

namespace Handyman_Studio
{
    public static class AccessCheck
    {
        private static ApplicationDbContext db = new ApplicationDbContext();

        public static bool Read(string userName, int panelId)
        {
            var userGroup = db.Users.Where(u => u.Email == userName).FirstOrDefault().GroupId;
            return db.GroupsPanels.Where(gp => gp.GroupId == userGroup && gp.PanelId == panelId).FirstOrDefault().Read;
        }

        public static bool Write(string userName, int panelId)
        {
            var userGroup = db.Users.Where(u => u.Email == userName).FirstOrDefault().GroupId;
            return db.GroupsPanels.Where(gp => gp.GroupId == userGroup && gp.PanelId == panelId).FirstOrDefault().Write;
        }

    }
}
