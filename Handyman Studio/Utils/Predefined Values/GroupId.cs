﻿namespace Handyman_Studio.Utils
{
    public static class GroupId
    {
        public const int Administrator = 1;
        public const int Manager = 2;
        public const int Accountant = 3;
        public const int Serviceman = 4;
        public const int Client = 5;
    }
}