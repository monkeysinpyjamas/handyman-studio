﻿namespace Handyman_Studio.Utils
{
    public static class PanelId
    {
        public const int FaultRegistration = 1;
        public const int Users = 2;
        public const int Groups = 3;
        public const int Products = 4;
        public const int Faults = 5;
        public const int Parts = 6;
        public const int Accounting = 7;
        public const int Logs = 8;
        public const int Calendar = 9;
    }
}