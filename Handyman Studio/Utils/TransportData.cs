﻿namespace Handyman_Studio.Utils
{
    public class TransportData
    {
        public class ClientData
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string PhoneNumer { get; set; }
            public string Mail { get; set; }
        }
    }
}