﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Channels;
using System.Web;
using System.Xml.Linq;
using Handyman_Studio.Models;

namespace Handyman_Studio.Utils
{
    public static class HtmlGenerator
    {
        public static string HtmlForFault(TransportData.ClientData client, Fault fault, IEnumerable<Part> faultParts)
        {
            var path = HttpContext.Current.Server.MapPath("/Utils/ReportTemplate.html");
            var html = File.ReadAllText(path);

            var partHtml = faultParts.Aggregate("",
                (current, part) => current + ("<tr class=\"item\"><td> " 
                                              + part.Name
                                              + "<br></td><td>" 
                                              + " 1 " 
                                              + " <br></td >" 
                                              + "<td class=\"cena\">" 
                                              + part.NetPrice.ToString("F2") 
                                              + "PLN<br></td></tr>"));

            html = html.Replace("[[FaultID]]", fault.FaultId.ToString());
            html = html.Replace("[[FaultReportDate]]", fault.FaultReportDate.ToString());
            html = html.Replace("[[FaultFixDate]]", fault.FaultRepairDate.ToString());

            html = html.Replace("[[ClientId]]", fault.UserId);
            html = html.Replace("[[ClientName]]", client.Name);
            html = html.Replace("[[ClientSurname]]", client.Surname);
            html = html.Replace("[[ClientMail]]", client.Mail);
            html = html.Replace("[[ClientNumber]]", client.PhoneNumer);

            html = html.Replace("[[Parts]]", partHtml);
            html = html.Replace("[[RepairPrice]]", fault.FaultRepairPrice.ToString("F2"));
            html = html.Replace("[[TotalPrice]]", (fault.FaultRepairPrice + fault.FaultPartPrice).ToString("F2"));

            return html;
        }
    }
}
